package jp.itnav.littlekidsworld;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import jp.itnav.littlekidsworld.data.CharacterData;
import jp.itnav.littlekidsworld.data.CharactersData;
import jp.itnav.littlekidsworld.data.Constants;
import jp.itnav.littlekidsworld.dialog.BaseCustomDialog;
import jp.itnav.littlekidsworld.dialog.CharacterDetailDialog;

public class CharacterListActivity extends BaseActivity {

    private CharacterDetailDialog   characterDetailDialog;
    private RecyclerView            recyclerView;
    private CharactersData          charactersData;
    private boolean                 isListItemClicked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_character_list);
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
    }

//    @Override
//    public void onRecyclerViewItemClick(View v, int position, CharacterData chara) {
//        if (!isListItemClicked) {
//            isListItemClicked = true;
//            characterDetailDialog.show(getFragmentManager(), "", chara, Constants.THEME_FOREST_OF_MONSTER);
//        }
//    }
//
//    private void setLayouts() {
//        if (charactersData != null) {
//            RecyclerViewAdapter adapter = new RecyclerViewAdapter(this, charactersData, this, 0);
//            recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
//            recyclerView.setLayoutManager(new GridLayoutManager(this, 4));
//            recyclerView.setHasFixedSize(true);
//            recyclerView.setVerticalScrollBarEnabled(true);
//            recyclerView.setAdapter(adapter);
//        }
//        characterDetailDialog = new CharacterDetailDialog();
//    }
//
//    @Override
//    public void onDismissDialogListener() {
//        isListItemClicked = false;
//    }
//
//    private void getCharactersData() {
//        charactersData = new CharactersData(Constants.getCharactersJSON(this));
//    }
}
