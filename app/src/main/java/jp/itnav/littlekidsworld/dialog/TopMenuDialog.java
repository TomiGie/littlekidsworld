package jp.itnav.littlekidsworld.dialog;


import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.widget.ImageButton;

import jp.itnav.littlekidsworld.CandyWorldActivity;
import jp.itnav.littlekidsworld.ForestOfMonsterActivity;
import jp.itnav.littlekidsworld.R;
import jp.itnav.littlekidsworld.data.Constants;


public class TopMenuDialog extends BaseCustomDialog implements View.OnClickListener {
    private ImageButton monsterOfForestButton;
    private ImageButton wonderZooButton;
    private Context context;

    @Override
    public void onPause() {
        super.onPause();
        if (activityDialog.isShowing()) {
//            activityDialog.dismiss();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected int getLayout() {
        return R.layout.dialog_top_menu;
    }

    @Override
    protected double getWidthRatio() {
        return 0.7;
    }

    @Override
    protected double getHeightRatio() {
        return 0.9;
    }

    @Override
    protected void setLayout(Dialog dialog) {
        super.setLayout(dialog);
        context = dialog.getContext();
        monsterOfForestButton = (ImageButton) dialog.findViewById(R.id.button_forest_of_monster);
        wonderZooButton = (ImageButton) dialog.findViewById(R.id.button_candy_world);
        monsterOfForestButton.setOnClickListener(this);
        wonderZooButton.setOnClickListener(this);
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    @Override
    public void onClick(View v) {
        playClickSound();
        int id = v.getId();
        Intent intent = null;
        switch (id) {
            case R.id.button_forest_of_monster:
                intent = createIntent(Constants.THEME_FOREST_OF_MONSTER, ForestOfMonsterActivity.class);
                break;
            case R.id.button_candy_world:
                intent = createIntent(Constants.THEME_CANDY_WORLD, CandyWorldActivity.class);
                break;
        }
        if (intent != null) {
            startActivity(intent);
        }
    }

    public void show(FragmentManager manager, String tag) {
        if (manager != null) {
            super.show(manager, tag);
        }
    }

    private Intent createIntent(int theme, Class<?> activity) {
        Intent intent = new Intent(getActivity().getApplicationContext(), activity);
        intent.putExtra(Constants.INTENT_KEY_THEME, theme);
        return intent;
    }
}
