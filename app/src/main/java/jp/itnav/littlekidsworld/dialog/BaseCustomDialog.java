package jp.itnav.littlekidsworld.dialog;


import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.SoundPool;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;

import jp.itnav.littlekidsworld.R;
import jp.itnav.littlekidsworld.data.Constants;

public abstract class BaseCustomDialog extends DialogFragment {
    protected Dialog activityDialog;
    protected SoundPool soundPool;
    protected int clickSoundId;
    protected Context context;

    protected abstract int getLayout();
    protected abstract double getWidthRatio();
    protected abstract double getHeightRatio();

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        activityDialog = new Dialog(getActivity());
        setLayout(activityDialog);
        return activityDialog;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Dialog dialog = getDialog();
        WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int dialogWidth = (int) (metrics.widthPixels * getWidthRatio());
        int dialogHeight = (int) (metrics.heightPixels * getHeightRatio());

        context = dialog.getContext();
        soundPool = Constants.buildSoundPool(1);
        clickSoundId = soundPool.load(context, R.raw.btn_click, 0);

        lp.width = dialogWidth;
        lp.height = dialogHeight;
        dialog.getWindow().setAttributes(lp);
    }

    protected void setLayout(Dialog dialog) {
        dialog.getWindow().requestFeature(getRequestFeature());
        dialog.setContentView(getLayout());
        dialog.getWindow().setBackgroundDrawable(getBackGroundDrawable());
    }

    protected ColorDrawable getBackGroundDrawable() {
        return new ColorDrawable(Color.TRANSPARENT);
    }

    protected int getRequestFeature() {
        return Window.FEATURE_NO_TITLE;
    }

    protected void playClickSound() {
        soundPool.play(clickSoundId, 1, 1, 0, 0, 1);
    }
}
