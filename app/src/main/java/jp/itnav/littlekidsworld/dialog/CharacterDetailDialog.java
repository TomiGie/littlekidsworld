package jp.itnav.littlekidsworld.dialog;


import android.app.Dialog;
import android.app.FragmentManager;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import jp.itnav.littlekidsworld.R;
import jp.itnav.littlekidsworld.RecyclerViewAdapter;
import jp.itnav.littlekidsworld.data.CharacterData;
import jp.itnav.littlekidsworld.data.CharacterImageData;
import jp.itnav.littlekidsworld.data.Constants;


public class CharacterDetailDialog extends BaseCustomDialog {

    protected Context context;
    private LinearLayout rootView;
    private TextView characterName;
    private TextView characterDescription;
    private TextView authorName;
    private TextView authorAge;
    private TextView authorAddress;
    private CharacterData characterData;
    private ImageView characterThumbnail;
    private ImageButton playButton;
    private ImageButton closeButton;
    private int dialogThemeId;
    private int selectedItemPosition;
    private CustomDialogCallBack listener;
    private RecyclerViewAdapter.ViewHolder viewHolder;

    public interface CustomDialogCallBack {
        void onDismissDialogListener();
        void onMonsterSelectedListener(int selectedPosition, CharacterData character, RecyclerViewAdapter.ViewHolder holder);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        activityDialog = super.onCreateDialog(savedInstanceState);
        activityDialog.setCancelable(false);
        return activityDialog;
    }

    @Override
    public void onPause() {
        super.onPause();
        if (activityDialog.isShowing()) {
            activityDialog.dismiss();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        listener.onDismissDialogListener();
    }

    @Override
    protected int getLayout() {
        return R.layout.dialog_monster_detail;
    }

    @Override
    protected double getWidthRatio() {
        return 0.9;
    }

    @Override
    protected double getHeightRatio() {
        return 1.0;
    }

    @Override
    protected void setLayout(Dialog dialog) {
        super.setLayout(dialog);
        rootView = (LinearLayout) dialog.findViewById(R.id.dialog_root_monster_status);
        characterName = (TextView) dialog.findViewById(R.id.text_dialog_character_detail_title);
        characterDescription = (TextView) dialog.findViewById(R.id.text_dialog_character_detail_desc);
        authorName = (TextView) dialog.findViewById(R.id.text_dialog_character_detail_author);
        authorAge = (TextView) dialog.findViewById(R.id.text_dialog_character_detail_age);
        authorAddress = (TextView) dialog.findViewById(R.id.text_dialog_character_detail_address);
        characterThumbnail = (ImageView) dialog.findViewById(R.id.dialog_monster_detail_image);
        playButton = (ImageButton) dialog.findViewById(R.id.btn_dialog_monster_detail_read);
        playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listener.onMonsterSelectedListener(selectedItemPosition, characterData, viewHolder);
                boolean isShowingDeleteIcon = (boolean) viewHolder.deleteImage.getTag();
                if (isShowingDeleteIcon) {
                    viewHolder.deleteImage.setImageResource(0);
                }
                else {
                    viewHolder.deleteImage.setImageResource(R.mipmap.btn_delete);
                }
                viewHolder.deleteImage.setTag(!isShowingDeleteIcon);
                dismiss();
            }
        });
        closeButton = (ImageButton) dialog.findViewById(R.id.btn_dialog_monster_detail_close);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        setMonsterData(characterData);
        setDialogTheme(dialogThemeId);
    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    public void show(FragmentManager manager, String tag, CharacterData monster, int themeId,
                     int position, CustomDialogCallBack listener, RecyclerViewAdapter.ViewHolder holder) {
        characterData = monster;
        dialogThemeId = themeId;
        selectedItemPosition = position;
        this.listener = listener;
        viewHolder = holder;
        super.show(manager, tag);
    }

    protected void setMonsterData(CharacterData character) {
        if (character == null) {
            return;
        }
        int characterId = character.getCharacterIntId();
        CharacterImageData images = new CharacterImageData(characterId);
        character.setIconResourceId(images.getCharacterImage());
        characterName.setText(character.getCharacterName());
        characterDescription.setText(character.getCharacterDescription());
        authorName.setText(character.getAuthorName());
        authorAge.setText(character.getAuthorAge() + "さい");
        authorAddress.setText(character.getAddress());
        characterThumbnail.setImageResource(character.getIconResourceId());
    }

    protected void setDialogTheme(int themeId) {
        switch (themeId) {
            case Constants.THEME_FOREST_OF_MONSTER:
                rootView.setBackgroundResource(R.mipmap.bg_popup_status_monster);
                playButton.setImageResource(R.mipmap.btn_play);
                closeButton.setImageResource(R.mipmap.btn_close_status_monster);
                break;
            case Constants.THEME_CANDY_WORLD:
                rootView.setBackgroundResource(R.mipmap.bg_popup_status_candy_world);
                playButton.setImageResource(R.mipmap.btn_play_candy_world);
                closeButton.setImageResource(R.mipmap.btn_close_status_candy_world);
                break;
        }
    }
}
