package jp.itnav.littlekidsworld;

import android.os.Bundle;

import jp.itnav.littlekidsworld.data.Constants;

public class CandyWorldActivity extends FieldBaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candy_world);
    }

    @Override
    protected int getThemeId() {
        return Constants.THEME_CANDY_WORLD;
    }

    @Override
    protected int getWorldId() {
        return Constants.WORLD_ID_CANDY_WORLD;
    }
}
