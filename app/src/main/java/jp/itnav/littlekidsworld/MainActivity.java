package jp.itnav.littlekidsworld;

import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageButton;

import jp.itnav.littlekidsworld.dialog.TopMenuDialog;

public class MainActivity extends BaseActivity {
    TopMenuDialog menuDialog;
    ImageButton buttonDoor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_main);
        setLayout();
    }

    protected void setLayout() {
        menuDialog = new TopMenuDialog();
        buttonDoor = (ImageButton) findViewById(R.id.button_top_door);
        buttonDoor.setOnClickListener(this);
    }

    protected void showMenuDialog() {
        menuDialog.show(getFragmentManager(), "");
    }

    @Override
    public void onClick(View v) {
        super.onClick(v);
        showMenuDialog();
    }
}
