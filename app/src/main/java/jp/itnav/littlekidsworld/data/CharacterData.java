package jp.itnav.littlekidsworld.data;

import android.graphics.Bitmap;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

public class CharacterData {
    private String characterId;
    private int stageId;
    private String characterName;
    private String authorName;
    private String authorSex;
    private String authorAge;
    private String authorAddress;
    private ArrayList<String> characterStories;
    private String characterDescription;
    private int characterResourceId;
    private Bitmap characterThumbnail;
    private int characterAnimationType;
    private int characterSoundId;

    public void setCharacterId(String id) {
        this.characterId = id;
    }

    public void setStageId(String stageId) {
        this.stageId = Integer.parseInt(stageId);
    }

    public void setCharacterName(String title) {
        characterName = title;
    }

    public void setAuthorName(String name) {
        authorName = name;
    }

    public void setAuthorSex(String sex) {
        authorSex = sex;
    }

    public void setAuthorAge(String age) {
        authorAge = age;
    }

    public void setAddress(String address) {
        authorAddress = address;
    }

    public void setCharacterStories(JSONArray stories) {
        if (characterStories == null) {
            characterStories = new ArrayList<>();
        }
        for (int i = 0; i < stories.length(); i++) {
            try {
                characterStories.add(i, stories.getString(i));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void setCharacterDescription(String description) {
        characterDescription = description;
    }

    public void setIconResourceId(int resourceId) {
        characterResourceId = resourceId;
    }

    public void setCharacterThumbnail(Bitmap thumb) {
        characterThumbnail = thumb;
    }

    public void setCharacterAnimationType(String id) {
        characterAnimationType = Integer.parseInt(id);
    }

    public void setCharacterSoundId(String soundId) {
        characterSoundId = Integer.parseInt(soundId);
    }

    public String getCharacterId() {
        return characterId;
    }

    public int getCharacterIntId() {
        return Integer.parseInt(characterId);
    }

    public int getStageId() {
        return stageId;
    }

    public String getCharacterName() {
        return characterName;
    }

    public String getAuthorName() {
        if (authorSex.equals("")) {
            return authorName;
        }
        else {
            String sexName = "";
            if (authorSex.equals("male")) {
                sexName = "くん";
            }
            else if (authorSex.equals("female")) {
                sexName = "ちゃん";
            }
            return authorName + sexName;
        }
    }

    public String getAuthorSex() {
        return authorSex;
    }

    public String getAuthorAge() {
        return authorAge;
    }

    public String getAddress() {
        return authorAddress;
    }

    public ArrayList<String> getStories() {
        return characterStories;
    }

    public String getStory(int pageIndex) {
        if (characterStories.size() == 0 || characterStories.size() < pageIndex) {
            return "";
        }
        return characterStories.get(pageIndex);
    }

    public String getCharacterDescription() {
        return characterDescription;
    }

    public int getIconResourceId() {
        return characterResourceId;
    }

    public Bitmap getThumbNail() {
        return characterThumbnail;
    }

    public int getCharacterAnimationType() {
        return characterAnimationType;
    }

    public int getCharacterSoundId() {
        return characterSoundId;
    }
}
