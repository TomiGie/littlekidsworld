package jp.itnav.littlekidsworld.data;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.IllegalFormatCodePointException;

public class CharactersData {
    private JSONObject mCharactersJsonObject;
    private ArrayList<CharacterData> mCharactersDataArray;

    public CharactersData(JSONObject json) {
        mCharactersJsonObject = json;
        mCharactersDataArray = new ArrayList<>();
        setCharacterData();
    }

    public void setCharacterData() {
        try {
            JSONArray characterDataArray = mCharactersJsonObject.getJSONArray(Constants.JSON_KEY_CHARACTER);
            for (int i = 0; i < characterDataArray.length(); i++) {
                mCharactersDataArray.add(i, parseData(characterDataArray.getJSONObject(i)));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<CharacterData> getCharactersData() {
        return mCharactersDataArray;
    }

    public ArrayList<CharacterData> getCharactersData(int worldId) {
        ArrayList<CharacterData> worldCharacterList = new ArrayList<>();
        for (CharacterData data : getCharactersData()) {
            if (data.getStageId() == worldId) {
                worldCharacterList.add(data);
            }
        }
        return worldCharacterList;
    }

    public CharacterData getCharacterData(int index) {
        return mCharactersDataArray.get(index);
    }

    public int getDataSize() {
        return mCharactersDataArray.size();
    }

    public int getDataSize(int worldId) {
        if (worldId == 0) {
            return getDataSize();
        }
        ArrayList<CharacterData> stageCharacterList = getCharactersData(worldId);
//        for (CharacterData data : getCharactersData()) {
//            if (data.getStageId() == worldId) {
//                stageCharacterList.add(data);
//                System.out.println("getDataSize world id --- " + stageCharacterList.size());
//            }
//        }
        return stageCharacterList.size();
    }

    public CharacterData parseData(JSONObject json) {
        CharacterData characterData = new CharacterData();
        try {
            characterData.setCharacterId(json.getString(Constants.JSON_KEY_CHARACTER_ID));
            characterData.setStageId(json.getString(Constants.JSON_KEY_WORLD));
            characterData.setCharacterName(json.getString(Constants.JSON_KEY_CHARACTER_TITLE));
            characterData.setAuthorName(json.getString(Constants.JSON_KEY_AUTHOR_NAME));
            characterData.setAuthorSex(json.getString(Constants.JSON_KEY_AUTHOR_SEX));
            characterData.setAuthorAge(json.getString(Constants.JSON_KEY_AUTHOR_AGE));
            characterData.setAddress(json.getString(Constants.JSON_KEY_AUTHOR_ADDRESS));
            characterData.setCharacterDescription(json.getString(Constants.JSON_KEY_CHARACTER_DESCRIPTION));
            characterData.setCharacterAnimationType(json.getString(Constants.JSON_KEY_CHARACTER_ANIMATION_TYPE));
            characterData.setCharacterSoundId(json.getString(Constants.JSON_KEY_CHARACTER_SOUND_ID));

//            JSONArray stories   = json.getJSONArray(Constants.JSON_KEY_CHARACTER_STORY);
//            characterData.setCharacterStories(stories);

//            if (Constants.DEBUG_MODE) {
//                Constants.putLogD("CharactersData", " ---- DATA ---- ");
//                Constants.putLogD("CharactersData", "id: " + characterData.getCharacterId());
//                Constants.putLogD("CharactersData", "title: " + characterData.getCharacterName());
//                Constants.putLogD("CharactersData", "desc: " + characterData.getCharacterDescription());
//                Constants.putLogD("CharactersData", "name: " + characterData.getAuthorName());
//                Constants.putLogD("CharactersData", "sex: " + characterData.getAuthorSex());
//                Constants.putLogD("CharactersData", "age: " + characterData.getAuthorAge());
//                Constants.putLogD("CharactersData", "address: " + characterData.getAddress());
//                Constants.putLogD("CharactersData", " -------------- ");
//            }
        }
        catch (JSONException e) {
            e.printStackTrace();
        }
        return characterData;
    }
}
