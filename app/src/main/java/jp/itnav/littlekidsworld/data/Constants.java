package jp.itnav.littlekidsworld.data;


import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.util.Log;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import jp.itnav.littlekidsworld.R;

public class Constants {
    public static final boolean DEBUG_MODE = true;

    // World Id
    public static final int WORLD_ID_FOREST_OF_MONSTER = 1000;
    public static final int WORLD_ID_CANDY_WORLD = 1001;

    // Json Parse key
    public static final String JSON_KEY_CHARACTER = "Characters";
    public static final String JSON_KEY_WORLD = "World";
    public static final String JSON_KEY_CHARACTER_ID = "Id";
    public static final String JSON_KEY_CHARACTER_TITLE = "Character_Name";
    public static final String JSON_KEY_AUTHOR_NAME = "Author_Name";
    public static final String JSON_KEY_AUTHOR_SEX = "Sex";
    public static final String JSON_KEY_AUTHOR_AGE = "Age";
    public static final String JSON_KEY_AUTHOR_ADDRESS = "Address";
    public static final String JSON_KEY_CHARACTER_DESCRIPTION = "Description";
    public static final String JSON_KEY_CHARACTER_SOUND_ID = "SoundId";
    public static final String JSON_KEY_CHARACTER_ANIMATION_TYPE = "AnimationType";

    // Intent Key
    public static final String INTENT_KEY_CHARACTER_ID = "characterId";

    // Application Theme
    public static final String INTENT_KEY_THEME = "Theme";
    public static final int THEME_FOREST_OF_MONSTER = 100;
    public static final int THEME_CANDY_WORLD = 101;

    public static final String THEME_TEXT_COLOR_FOREST_OF_MONSTER = "#1C3B44";
    public static final String THEME_TEXT_COLOR_CANDY_WORLD = "#8B324F";

    public static void putLogD(String tag, String msg) {
        if (DEBUG_MODE) {
            Log.d(tag, msg);
        }
    }

    public static void showToast(Context c, String msg, int duration) {
        Toast.makeText(c, msg, duration).show();
    }

    @SuppressWarnings("deprecation")
    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public static SoundPool buildSoundPool(int poolMax) {
        SoundPool pool = null;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            pool = new SoundPool(poolMax, AudioManager.STREAM_MUSIC, 0);
        }
        else {
            AudioAttributes attr = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_MEDIA)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build();

            pool = new SoundPool.Builder()
                    .setAudioAttributes(attr)
                    .setMaxStreams(poolMax)
                    .build();
        }

        return pool;
    }

    public static JSONObject getCharactersJSON(Context context) {
        InputStream inputStream = null;
        BufferedReader bufferedReader = null;
        String text = "";
        AssetManager assetManager = context.getResources().getAssets();
        JSONObject json = null;

        try {
            try {
                inputStream = assetManager.open("characters_data.json");
                bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                String str;
                while ((str = bufferedReader.readLine()) != null) {
                    text += str + "\n";
                }

                json = new JSONObject(text);

            } finally {
                if (inputStream != null) inputStream.close();
                if (bufferedReader != null) bufferedReader.close();
            }
        } catch (Exception e) {
            Constants.putLogD("", "Exception!!" + e);
        }

        if (json == null) {
            json = new JSONObject();
        }
        return json;
    }
}
