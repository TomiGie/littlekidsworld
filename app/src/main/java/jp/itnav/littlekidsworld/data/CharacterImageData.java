package jp.itnav.littlekidsworld.data;


import jp.itnav.littlekidsworld.R;

public class CharacterImageData {
    private final int characterId;
    private final int charactersImage[] = {
            R.mipmap.character_01,
            R.mipmap.character_02,
            R.mipmap.character_03,
            R.mipmap.character_04,
            R.mipmap.character_05,
            R.mipmap.character_06,
            R.mipmap.character_07,
            R.mipmap.furuyama,
            R.mipmap.character_08,
            R.mipmap.character_09,
            R.mipmap.character_10,
            R.mipmap.character_11,
            R.mipmap.character_12,
            R.mipmap.character_13,
            R.mipmap.character_14,
            R.mipmap.character_15,
            R.mipmap.character_16,
            R.mipmap.character_17,
            R.mipmap.character_18,
            R.mipmap.character_19,
            R.mipmap.character_20,
            R.mipmap.character_21,
            R.mipmap.character_22,
            R.mipmap.character_23,
            R.mipmap.character_24,
            R.mipmap.character_25,
            R.mipmap.character_26,
            R.mipmap.character_27,
            R.mipmap.character_28
    };

//    private final int mCharactersIcon[] = {
//            R.mipmap.ic_launcher,
//            R.mipmap.ic_launcher,
//            R.mipmap.ic_launcher,
//            R.mipmap.ic_launcher,
//            R.mipmap.ic_launcher,
//            R.mipmap.ic_launcher,
//            R.mipmap.ic_launcher,
//            R.mipmap.ic_launcher,
//            R.mipmap.ic_launcher,
//            R.mipmap.ic_launcher,
//            R.mipmap.ic_launcher,
//            R.mipmap.ic_launcher,
//            R.mipmap.ic_launcher,
//            R.mipmap.ic_launcher,
//            R.mipmap.monster01,
//            R.mipmap.monster01
//    };

    public CharacterImageData(int id) {
        characterId = id;
    }

//    public int[][] getCharactersImageArray() {
//        return charactersImage;
//    }
//
//    public int[] getCharacterImagesArray() {
//        return charactersImage[characterId];
//    }

    public int getCharacterImage() {
        return charactersImage[characterId];
    }

    public int getCharacterImageLength() {
//        return charactersImage[characterId].length;
        return charactersImage.length;
    }

//    public int getCharacterIcon(int index) {
//        return mCharactersIcon[index];
//    }
}
