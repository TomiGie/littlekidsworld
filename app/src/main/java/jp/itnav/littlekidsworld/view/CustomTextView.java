package jp.itnav.littlekidsworld.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import jp.itnav.littlekidsworld.R;
import jp.itnav.littlekidsworld.data.Constants;


public class CustomTextView extends TextView {

    public CustomTextView(Context context) {
        super(context);
        Typeface face=Typeface.createFromAsset(context.getAssets(), "keifont.ttf");
        this.setTypeface(face);
        setTextColor(Color.WHITE);
    }

    public CustomTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface face=Typeface.createFromAsset(context.getAssets(), "keifont.ttf");
        this.setTypeface(face);
        setTextColor(Color.WHITE);
    }

    public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Typeface face=Typeface.createFromAsset(context.getAssets(), "keifont.ttf");
        this.setTypeface(face);
        setTextColor(Color.WHITE);
    }

    protected void onDraw (Canvas canvas) {
        super.onDraw(canvas);
    }

    public void setColor(int themeId) {
        String textColor = "#FFFFFF";
        switch (themeId) {
            case Constants.THEME_FOREST_OF_MONSTER:
                textColor = Constants.THEME_TEXT_COLOR_FOREST_OF_MONSTER;
                break;
            case Constants.THEME_CANDY_WORLD:
                textColor = Constants.THEME_TEXT_COLOR_CANDY_WORLD;
                break;
        }
        setTextColor(Color.parseColor(textColor));
    }
}