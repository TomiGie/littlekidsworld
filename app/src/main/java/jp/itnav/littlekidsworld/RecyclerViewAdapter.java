package jp.itnav.littlekidsworld;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import java.util.ArrayList;

import jp.itnav.littlekidsworld.data.CharacterData;
import jp.itnav.littlekidsworld.data.CharacterImageData;
import jp.itnav.littlekidsworld.data.CharactersData;
import jp.itnav.littlekidsworld.data.Constants;
import jp.itnav.littlekidsworld.view.CustomTextView;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    public interface OnRecyclerViewClickListener {
        void onRecyclerViewItemClick(View v, int position, CharacterData character, RecyclerViewAdapter.ViewHolder holder);
    }

    private LayoutInflater layoutInflater;
    private CharactersData charactersData;
    private OnRecyclerViewClickListener onRecyclerViewClickListener;
    private int themeId;
    private int worldId;


    public RecyclerViewAdapter(Context context, CharactersData data, OnRecyclerViewClickListener listener, int themeId, int worldId) {
        super();
        layoutInflater = LayoutInflater.from(context);
        charactersData = data;
        onRecyclerViewClickListener = listener;
        this.themeId = themeId;
        this.worldId = worldId;
    }

    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = layoutInflater.inflate(R.layout.card, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final RecyclerViewAdapter.ViewHolder holder, final int position) {
        ArrayList<CharacterData> dataList = charactersData.getCharactersData(worldId);
//        final CharacterData chara = charactersData.getCharacterData(position);
        final CharacterData chara = dataList.get(position);
        CharacterImageData images = new CharacterImageData(Integer.parseInt(chara.getCharacterId()));
        int imageLength = images.getCharacterImageLength() - 1;
        String name = chara.getCharacterName();
        holder.monsterName.setText(name);
        holder.characterIcon.setImageResource(images.getCharacterImage());
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onRecyclerViewClickListener.onRecyclerViewItemClick(v, position, chara, holder);
            }
        });
        setAdapterTheme(holder, themeId);
    }

    @Override
    public int getItemCount() {
        return charactersData.getDataSize(worldId);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public CardView cardView;
        public FrameLayout monsterBgFrame;
        public RecyclerView recyclerView;
        public ImageView characterIcon;
        public ImageView deleteImage;
        public CustomTextView monsterName;

        @SuppressLint("NewApi")
        public ViewHolder(View v) {
            super(v);
            monsterBgFrame = (FrameLayout) v.findViewById(R.id.card_frame_monster);
            cardView = (CardView) v.findViewById(R.id.cardView);
            recyclerView = (RecyclerView) v.findViewById(R.id.recycler_view);
            characterIcon = (ImageView) v.findViewById(R.id.cardArticleImage);
            deleteImage = (ImageView) v.findViewById(R.id.card_article_image_delete);
            monsterName = (CustomTextView) v.findViewById(R.id.card_monster_name);
            deleteImage.setTag(false);

            int version = Build.VERSION.SDK_INT;
            if (version >= Build.VERSION_CODES.LOLLIPOP) {
                cardView.setElevation(10);
            }
        }
    }

    private void setAdapterTheme(final RecyclerViewAdapter.ViewHolder holder, int themeId) {
        switch (themeId) {
            case Constants.THEME_FOREST_OF_MONSTER:
                holder.monsterBgFrame.setBackgroundResource(R.mipmap.frame_monster);
                break;
            case Constants.THEME_CANDY_WORLD:
                holder.monsterBgFrame.setBackgroundResource(R.mipmap.frame_candy_world);
        }
        holder.monsterName.setColor(themeId);
    }
}