package jp.itnav.littlekidsworld;


import android.media.SoundPool;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;

import jp.itnav.littlekidsworld.data.Constants;

public abstract class BaseActivity extends AppCompatActivity implements View.OnClickListener{
    protected SoundPool soundPool;
    protected int clickSoundId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setFullScreenMode();
    }

    @Override
    protected void onResume() {
        super.onResume();
        soundPool = Constants.buildSoundPool(10);
        clickSoundId = soundPool.load(this, R.raw.btn_click, 1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        soundPool.release();
    }

    @Override
    public void onClick(View v) {
        playClickSound();
    }

    protected void setFullScreenMode() {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    protected void playClickSound() {
        soundPool.play(clickSoundId, 1, 1, 0, 0, 1);
    }
}