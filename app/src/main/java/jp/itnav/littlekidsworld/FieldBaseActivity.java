package jp.itnav.littlekidsworld;


import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.RotateAnimation;
import android.view.animation.TranslateAnimation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import jp.itnav.littlekidsworld.data.CharacterData;
import jp.itnav.littlekidsworld.data.CharactersData;
import jp.itnav.littlekidsworld.data.Constants;
import jp.itnav.littlekidsworld.dialog.CharacterDetailDialog;

public abstract class FieldBaseActivity extends BaseActivity implements
        RecyclerViewAdapter.OnRecyclerViewClickListener, CharacterDetailDialog.CustomDialogCallBack {

    protected static final int FIELD_MONSTER_COLUMN_NUMBER = 4;
    protected CharacterDetailDialog characterDetailDialog;
    protected RecyclerView recyclerView;
    protected RelativeLayout fieldRootView;
    protected CharactersData charactersData;
    protected boolean isListItemClicked;
    protected boolean isMonsterListShowing;
    protected ImageView monsterImageLeft;
    protected ImageView monsterImageCenter;
    protected ImageView monsterImageRight;
    protected ImageButton characterListButton;
    protected int moveYDelta;
    protected int currentThemeId;
    protected ArrayList<CharacterData> characterList;
    protected ArrayList<RecyclerViewAdapter.ViewHolder> viewHolders;
    protected ArrayList<Animation> animationArrayList;
    protected ArrayList<Integer> soundResourceList;

    protected int[] animationResources = {
            R.anim.anim_monster_01,
            R.anim.anim_monster_02,
            R.anim.anim_monster_03,
            R.anim.anim_monster_04,
            R.anim.anim_monster_05,
            R.anim.anim_monster_06,
            R.anim.anim_monster_07,
            R.anim.anim_monster_08
    };

    protected int[] soundResources = {
            R.raw.character_voice01,
            R.raw.character_voice02,
            R.raw.character_voice03,
            R.raw.character_voice04,
            R.raw.character_voice05,
            R.raw.character_voice06,
            R.raw.character_voice07,
            R.raw.character_voice08,
            R.raw.character_voice_micchie
    };

    protected abstract int getThemeId();

    protected abstract int getWorldId();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        currentThemeId = getThemeId();
        characterList = new ArrayList<>();
    }

    @Override
    protected void onResume() {
        super.onResume();
        isMonsterListShowing = false;
        loadAnimation();
        loadMedia();
        getCharactersData();
        setLayouts();
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        int soundId;
        switch (id) {
            case R.id.button_monster_list:
                onClickMonsterListButton();
                break;
            case R.id.image_monster_left:
                soundId = (int) monsterImageLeft.getTag(R.string.tag_view_sound_id);
                playCharacterVoice(soundId);
                break;
            case R.id.image_monster_center:
                soundId = (int) monsterImageCenter.getTag(R.string.tag_view_sound_id);
                playCharacterVoice(soundId);
                break;
            case R.id.image_monster_right:
                soundId = (int) monsterImageRight.getTag(R.string.tag_view_sound_id);
                playCharacterVoice(soundId);
                break;
        }
    }

    @Override
    public void onRecyclerViewItemClick(View v, int position, CharacterData chara, RecyclerViewAdapter.ViewHolder holder) {
        if (!isListItemClicked && isMonsterListShowing) {
            boolean shouldDeleteCharacter = (boolean) holder.deleteImage.getTag();
            if (shouldDeleteCharacter) {
                deleteFieldCharacter(chara.getCharacterIntId());
                holder.deleteImage.setTag(false);
                holder.deleteImage.setImageResource(0);
            }
            else {
                isListItemClicked = true;
                characterDetailDialog.show(getFragmentManager(), "", chara, currentThemeId, position, this, holder);
            }
            playClickSound();
        }
    }

    /**
     *  Dialog Callback Listener
     */
    @Override
    public void onDismissDialogListener() {
        isListItemClicked = false;
        playClickSound();
    }

    @Override
    public void onMonsterSelectedListener(int selectedPosition, CharacterData character, RecyclerViewAdapter.ViewHolder holder) {
        if (characterList == null) {
            characterList = new ArrayList<>();
        }

        if (viewHolders == null) {
            viewHolders = new ArrayList<>();
        }

        if (3 < characterList.size() + 1) {
            characterList.remove(0);
        }

        if (3 < viewHolders.size() + 1) {
            viewHolders.get(0).deleteImage.setImageResource(0);
            viewHolders.get(0).deleteImage.setTag(false);
            viewHolders.remove(0);
        }
        characterList.add(character);
        viewHolders.add(holder);
        updateFieldCharacterImages();
        moveField();
    }
    /***/

    @Override
    public void onBackPressed() {
        if (isMonsterListShowing) {
            onClickMonsterListButton();
            return;
        }
        super.onBackPressed();
    }

    protected void getCharactersData() {
        charactersData = new CharactersData(Constants.getCharactersJSON(this));
    }

    protected void loadAnimation() {
        animationArrayList = new ArrayList<>();
        for (int animationResource : animationResources) {
            animationArrayList.add(AnimationUtils.loadAnimation(this, animationResource));
        }
    }

    protected void loadMedia() {
        soundResourceList = new ArrayList<>();
        soundResourceList.add(0);
        for (int soundResource : soundResources) {
            soundResourceList.add(soundPool.load(this, soundResource, 1));
        }
    }

    protected void setLayouts() {
        monsterImageLeft = (ImageView) findViewById(R.id.image_monster_left);
        monsterImageCenter = (ImageView) findViewById(R.id.image_monster_center);
        monsterImageRight = (ImageView) findViewById(R.id.image_monster_right);
        fieldRootView = (RelativeLayout) findViewById(R.id.view_root_field);

        initializeFieldImageViews();

        characterListButton = (ImageButton) findViewById(R.id.button_monster_list);
        characterListButton.setOnClickListener(this);

        if (charactersData != null) {
            RecyclerViewAdapter adapter = new RecyclerViewAdapter(this, charactersData, this, currentThemeId, getWorldId());
            recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
            recyclerView.setLayoutManager(new GridLayoutManager(this, FIELD_MONSTER_COLUMN_NUMBER));
            recyclerView.setHasFixedSize(true);
            recyclerView.setVerticalScrollBarEnabled(true);
            recyclerView.setAdapter(adapter);
        }
        characterDetailDialog = new CharacterDetailDialog();
    }

    protected void deleteFieldCharacter(int characterId) {
        int leftCharacterId = (int) monsterImageLeft.getTag(R.string.tag_view_character_id);
        int centerCharacterId = (int) monsterImageCenter.getTag(R.string.tag_view_character_id);
        int rightCharacterId = (int) monsterImageRight.getTag(R.string.tag_view_character_id);

        if (characterId == leftCharacterId) {
            characterList.remove(0);
            viewHolders.remove(0);
        }
        else if (characterId == centerCharacterId) {
            characterList.remove(1);
            viewHolders.remove(1);
        }
        else if (characterId == rightCharacterId) {
            characterList.remove(2);
            viewHolders.remove(2);
        }
        updateFieldCharacterImages();
    }

    protected void updateFieldCharacterImages() {
        initializeFieldImageViews();
        for (int i = 0; i < characterList.size(); i++) {
            CharacterData data = characterList.get(i);
            int resourceId = data.getIconResourceId();
            int characterId = data.getCharacterIntId();
            int soundId = data.getCharacterSoundId();
            if (i == 0) {
                monsterImageLeft.setImageResource(resourceId);
                monsterImageLeft.setTag(R.string.tag_view_character_id, characterId);
                monsterImageLeft.setTag(R.string.tag_view_sound_id, soundId);
            }
            else if (i == 1) {
                monsterImageCenter.setImageResource(resourceId);
                monsterImageCenter.setTag(R.string.tag_view_character_id, characterId);
                monsterImageCenter.setTag(R.string.tag_view_sound_id, soundId);
            }
            else {
                monsterImageRight.setImageResource(resourceId);
                monsterImageRight.setTag(R.string.tag_view_character_id, characterId);
                monsterImageRight.setTag(R.string.tag_view_sound_id, soundId);
            }
        }
    }

    protected void initializeFieldImageViews() {
        monsterImageLeft.setTag(R.string.tag_view_character_id, -1);
        monsterImageCenter.setTag(R.string.tag_view_character_id, -1);
        monsterImageRight.setTag(R.string.tag_view_character_id, -1);
        monsterImageLeft.setTag(R.string.tag_view_sound_id, 0);
        monsterImageCenter.setTag(R.string.tag_view_sound_id, 0);
        monsterImageRight.setTag(R.string.tag_view_sound_id, 0);
        monsterImageLeft.setImageResource(0);
        monsterImageCenter.setImageResource(0);
        monsterImageRight.setImageResource(0);
        monsterImageLeft.setOnClickListener(this);
        monsterImageCenter.setOnClickListener(this);
        monsterImageRight.setOnClickListener(this);
    }

    protected void onClickMonsterListButton() {
        moveField();
    }

    protected void playCharacterVoice(int soundId) {
        if (soundId < 1) {
            return;
        }
        soundPool.play(soundResourceList.get(soundId), 1, 1, 0, 0, 1);
    }

    protected int getMoveHeight() {
        int height;
        if (isMonsterListShowing) {
            height = 0;
            resumeAnimation();
        } else {
            height = findViewById(R.id.monster_select_frame).getHeight();
            poseAnimation();
        }
        return height;
    }

    protected void resumeAnimation() {
        for (int i = 0; i < characterList.size(); i++) {
            // AnimationTypeは1から開始するのでCharacterDataから取得した
            // 値から-1をして、配列の取得する位置をあわせている
            Animation anim = animationArrayList.get(characterList.get(i).getCharacterAnimationType() - 1);
            if (i == 0) {
                monsterImageLeft.startAnimation(anim);
            }
            else if (i == 1) {
                monsterImageCenter.startAnimation(anim);
            }
            else if (i == 2) {
                monsterImageRight.startAnimation(anim);
            }
        }
    }

    protected void poseAnimation() {
        monsterImageLeft.clearAnimation();
        monsterImageCenter.clearAnimation();
        monsterImageRight.clearAnimation();
    }

    protected void moveField() {
        float fromY;
        final float toY;
        if (moveYDelta == 0) {
            moveYDelta = getMoveHeight();
        }
        if (isMonsterListShowing) {
            fromY = 0.0f;
            toY = moveYDelta;
        } else {
            fromY = 0.0f;
            toY = -moveYDelta;
        }
        Animation anim = new TranslateAnimation(0, 0, fromY, toY);
        anim.setDuration(200);
        anim.setRepeatCount(0);
        anim.setFillAfter(false);
        anim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                rotateButton();
                isMonsterListShowing = !isMonsterListShowing;
                if (isMonsterListShowing) {
                    fieldRootView.setTranslationY(toY);
                    poseAnimation();
                } else {
                    fieldRootView.setTranslationY(0);
                    resumeAnimation();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        fieldRootView.startAnimation(anim);
    }

    protected void rotateButton() {
        float fromDeg;
        float toDeg;
        if (isMonsterListShowing) {
            fromDeg = 45.0f;
            toDeg = 0.0f;
        } else {
            fromDeg = 0.0f;
            toDeg = 45.0f;
        }
        Animation an = new RotateAnimation(fromDeg, toDeg, characterListButton.getWidth() / 2, characterListButton.getHeight() / 2);
        an.setDuration(200);
        an.setRepeatCount(0);
        an.setFillAfter(true);
        characterListButton.startAnimation(an);
    }
}
