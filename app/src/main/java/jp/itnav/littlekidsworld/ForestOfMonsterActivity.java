package jp.itnav.littlekidsworld;

import android.os.Bundle;

import jp.itnav.littlekidsworld.data.Constants;

public class ForestOfMonsterActivity extends FieldBaseActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forest_of_monster);
    }

    @Override
    protected int getThemeId() {
        return Constants.THEME_FOREST_OF_MONSTER;
    }

    @Override
    protected int getWorldId() {
        return Constants.WORLD_ID_FOREST_OF_MONSTER;
    }
}
